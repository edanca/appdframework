from selenium.webdriver.support.wait import WebDriverWait
from utils import Helper

from config import setup
from selenium import webdriver

class Test_Base():

    def mySetUp(self, url=None):
        if url == None:
            self.baseURL = setup.baseURI
        else:
            self.baseURL = url

        self.driver = self.getBrowser()
        self.driver.get(self.baseURL)

        return self.driver

    def getBrowser(self):
        if setup.browser == 'Chrome':
            driver = webdriver.Chrome()
        elif setup.browser == 'Firefox':
            # Geckodriver needs to be installed
            driver = webdriver.Firefox()

        return driver

    def wait(self, time=10):
        WebDriverWait(self.driver, time, poll_frequency=1)

    def switch_browser_tab(self, driver, tab_number):
        Helper.switch_browser_tab(driver, tab_number)