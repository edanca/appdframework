import pytest
import time
import numpy as np
from base.test_base import Test_Base
from pages.forgotPasswordPage import ForgotPasswordPage
from pages.homePage import HomePage
from pages.loginPage import LoginPage
from pages.signUpPage import SignUpPage
from utils import Helper
from operations.actions import Actions


class Test_SignUp(Test_Base):

    @pytest.fixture(scope="module")
    def driver(self):
        try:
            driver = self.mySetUp()
            yield driver
        finally:
            driver.quit()

    @pytest.fixture(autouse=True)
    def before_after_testcase(self, driver):
        driver.implicitly_wait(10)
        driver.maximize_window()
        yield driver
        print(f'Test name: {self.test_name} - executed')
        print('========= FINISH =========')

    # =======================================================================

    def test_sign_up_success(self, driver):
        self.test_name = 'test_sign_up_success'

        email = Helper.randomString(20) + '@yopmail.com'

        actions = Actions()
        actions.signUp(driver, email)

        assert True
