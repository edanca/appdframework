from selenium.webdriver import ActionChains

from pages.forgotPasswordPage import ForgotPasswordPage
from pages.homePage import HomePage
from pages.loginPage import LoginPage
from pages.signUpPage import SignUpPage
from utils import *
from utils import Helper

class Actions():

    def signUp(self, driver, email):
        homePage = HomePage()
        homePage.click_login(driver)

        Helper.switch_browser_tab(driver, 1)

        loginPage = LoginPage()
        loginPage.click_forgot_password(driver)

        forgotPWPage = ForgotPasswordPage()
        forgotPWPage.click_register(driver)

        signUpPage = SignUpPage()
        signUpPage.enter_email(driver, email)
        signUpPage.press_registrarse(driver)

    def signUp2(self, driver, email):

        login_btn = HomePage.get_login_btn(driver)

        actions = ActionChains(driver)
        actions.click()

