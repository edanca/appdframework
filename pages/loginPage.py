from utils import Locator
from base.basePage import BasePage

class LoginPage(BasePage):

    css_forgotpassword_link = '.adb-caption.adb-form--caption > a'

    def click_forgot_password(self, driver):
        #css_selector = '.adb-caption.adb-form--caption > a'
        elem_forgotpassword_link = Locator._find_element_by_css(driver, self.css_forgotpassword_link)
        elem_forgotpassword_link.click()