from utils import Locator
from base.basePage import BasePage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

class SignUpPage(BasePage):

    xpath_email_field = '//input[@type="email"]'
    name_registrarse_link = 'userSignupButton'

    def enter_email(self, driver, email):
        elem_email_field = Locator._find_element_by_xpath(driver, self.xpath_email_field)
        elem_email_field.send_keys(email)

    def press_registrarse(self, driver):
        elem_registrarse_link = Locator._find_element_by_name(driver, self.name_registrarse_link)
        elem_registrarse_link.click()