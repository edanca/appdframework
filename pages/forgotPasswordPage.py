from utils import Locator, Logger
from base.basePage import BasePage

class ForgotPasswordPage(BasePage):

    _css_selector_register_btn = 'div.forgotPassword > span.linkLook'

    def click_register(self, driver):
        elem = Locator._find_element_by_css(driver, self._css_selector_register_btn)
        elem.click()