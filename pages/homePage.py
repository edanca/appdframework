from utils import Locator

class HomePage():

    css_login_btn = '#container > header > nav > ul:nth-child(4) > li > a'

    def click_login(self, driver):
        elem_login_btn = HomePage.get_login_btn(driver)
        elem_login_btn.click()

    @staticmethod
    def get_login_btn(driver):
        hp = HomePage()
        css_login_btn = hp.css_login_btn
        elem = Locator._find_element_by_css(driver, css_login_btn)
        return elem