from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


#******************************
#     Locators Singular
#******************************

def _find_element(driver, *args):
    # generic finder
    # *args = [By any type of selector, selector]
    try:
        elem = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located(*args)
        )
        return driver.find_element(*args)
    except NoSuchElementException as e:
        __print_no_such_elem()
        raise e

def _find_element_by_id(driver, id):
    try:
        elem = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.ID, id))
        )
        return driver.find_element_by_id(id)
    except NoSuchElementException as e:
        __print_no_such_elem()
        raise e

def _find_element_by_name(driver, name):
    try:
        elem = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.NAME, name))
        )
        return driver.find_element_by_name(name)
    except NoSuchElementException as e:
        __print_no_such_elem()
        raise e

def _find_element_by_class_name(driver, class_name):
    try:
        elem = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.CLASS_NAME, class_name))
        )
        return driver.find_element_by_class_name(class_name)
    except NoSuchElementException as e:
        __print_no_such_elem()
        raise e

def _find_element_by_css(driver, css_selector):
    try:
        elem = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, css_selector))
        )
        return driver.find_element_by_css_selector(css_selector)
    except NoSuchElementException as e:
        __print_no_such_elem()
        raise e

def _find_element_by_xpath(driver, xpath):
    try:
        elem = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.XPATH, xpath))
        )
        return driver.find_element_by_xpath(xpath)
    except NoSuchElementException as e:
        __print_no_such_elem()
        raise e

def _find_element_by_link_text(driver, link_text):
    try:
        elem = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located(((By.LINK_TEXT, link_text)))
        )
        return driver.find_element_by_link_text(link_text)
    except NoSuchElementException as e:
        __print_no_such_elem()
        raise e

#******************************
#     Locators Plural
#******************************

def _find_elements_by_css(driver, css_selector):
    try:
        elem = WebDriverWait(driver, 5).until(
            EC.presence_of_all_elements_located((By.CSS_SELECTOR, css_selector))
        )
        return driver.find_elements_by_css_selector(css_selector)
    except NoSuchElementException as e:
        __print_no_such_elem()
        raise e

def _find_elements_by_tag_name(driver, tag_name):
    try:
        elem = WebDriverWait(driver, 5).until(
            EC.presence_of_all_elements_located((By.TAG_NAME, tag_name))
        )
        return driver.find_elements_by_tag_name(tag_name)
    except NoSuchElementException as e:
        __print_no_such_elem()
        raise e

def _find_elements_by_link_text(driver, link_text):
    try:
        elem = WebDriverWait(driver, 5).until(
            EC.presence_of_all_elements_located((By.LINK_TEXT, link_text))
        )
        return driver.find_elements_by_link_text(link_text)
    except NoSuchElementException as e:
        __print_no_such_elem()
        raise e

#******************************
#     Private Functions
#******************************
def __print_no_such_elem():
    print('======== NO SUCH ELEMENT FOUND =========')
