import string
from random import *

def randomString(lenght):
    str_type = string.ascii_letters + string.digits
    random_str = ''.join(choice(str_type) for i in range(randint(lenght, lenght)))
    return random_str

def switch_browser_tab(driver, tab_number):
    tabs = driver.window_handles
    driver.switch_to.window(tabs[tab_number])